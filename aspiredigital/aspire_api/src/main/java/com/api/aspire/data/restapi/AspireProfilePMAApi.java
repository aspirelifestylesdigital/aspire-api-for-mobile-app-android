package com.api.aspire.data.restapi;

import com.api.aspire.data.entity.userprofile.pma.ProfilePMAResponse;
import com.api.aspire.data.entity.userprofile.pma.SearchProfilePMAResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AspireProfilePMAApi {

    @Headers({"Content-Type: application/json"})
    @GET("parties/search")
    Call<List<SearchProfilePMAResponse>> searchPartyPerson(@Header("Authorization") String tokenPMA,
                                                           @Query("applicationCode") String applicationCode,
                                                           @Query("parentOrgCode") String parentOrgCode,
                                                           @Query("emailAddress") String email);

    @Headers({"Content-Type: application/json"})
    @GET("parties/{partyId}?contacts=true&locations=true")
    Call<ProfilePMAResponse> getProfilePMA(@Header("Authorization") String tokenPMA,
                                           @Path("partyId") String partyId,
                                           @Query("applicationCode") String applicationCode,
                                           @Query("parentOrgCode") String parentOrgCode);

}