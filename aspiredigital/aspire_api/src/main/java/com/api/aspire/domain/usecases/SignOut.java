package com.api.aspire.domain.usecases;

import com.api.aspire.data.preference.PreferencesStorageAspire;


public class SignOut {

    private PreferencesStorageAspire preferencesStorageAspire;

    public SignOut(PreferencesStorageAspire preferencesStorageAspire) {
        this.preferencesStorageAspire = preferencesStorageAspire;
    }

    public void setSignOutProfile(OnSignOutListener onSignOut) {
        preferencesStorageAspire.clear();
        if (onSignOut != null) {
            onSignOut.onSignOut();
        }
    }

    /**
     * Callback to reset CityData: remove select city data
     * Use this line: CityData.reset();
     */
    public interface OnSignOutListener {
        void onSignOut();
    }
}
