package com.api.aspire.domain.repository;

import com.api.aspire.domain.model.UserProfileOKTA;

import io.reactivex.Single;

public interface UserProfileOKTARepository {

    Single<UserProfileOKTA> getUserProfileOKTA(String email);

    boolean isStatusLockOut(String status);
}
