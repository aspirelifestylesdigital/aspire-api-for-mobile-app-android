package com.api.aspire.data.entity.profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 6/6/18.
 */
public class UpdateProfileAspireRequest extends ProfileAspireResponse {

    @SerializedName("verificationMetadata")
    private VerificationMetadata verificationMetadata;

    public UpdateProfileAspireRequest(int bin) {
        this.verificationMetadata = new VerificationMetadata(bin);
    }

    public void setBinVerificationMetadata(int bin){
        verificationMetadata.setBin(bin);
    }
}
