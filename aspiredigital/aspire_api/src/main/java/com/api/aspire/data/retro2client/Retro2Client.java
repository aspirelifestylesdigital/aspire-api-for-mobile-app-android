package com.api.aspire.data.retro2client;

import android.text.TextUtils;

import com.api.aspire.BuildConfig;
import com.api.aspire.common.SSLSolverOkHttpClientCore;

import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class Retro2Client {
    private final String TAG = Retro2Client.class.getSimpleName();

    /**
     * use for normal api
     * */
    protected OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okHttpBuilder = buildOkHttpClientBuilder();
        SSLSolverOkHttpClientCore.solve(okHttpBuilder);
        return okHttpBuilder.build();
    }

    /**
     * use for api BasicAuth
     * */
    protected OkHttpClient provideOkHttpClientBasicAuth(String userName, String secret) {
        OkHttpClient.Builder okHttpBuilder = buildOkHttpClientBuilder();
        //Basic Auth
        String authToken = "";
        if (!TextUtils.isEmpty(userName)
                && !TextUtils.isEmpty(secret)) {
            authToken = Credentials.basic(userName, secret);
        }
        //Create a new Interceptor.
        final String finalAuthToken = authToken;
        Interceptor headerAuthorizationInterceptor = chain -> {
            okhttp3.Request request = chain.request();
            Headers headers = request.headers().newBuilder().add("Authorization", finalAuthToken).build();
            request = request.newBuilder().headers(headers).build();
            return chain.proceed(request);
        };
        //Add the interceptor to the client builder.
        okHttpBuilder.addInterceptor(headerAuthorizationInterceptor);

        SSLSolverOkHttpClientCore.solve(okHttpBuilder);
        return okHttpBuilder.build();
    }

    private OkHttpClient.Builder buildOkHttpClientBuilder() {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            okHttpBuilder.addInterceptor(provideHttpLoggingInterceptor());
        }

        okHttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okHttpBuilder.readTimeout(60, TimeUnit.SECONDS);
        okHttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        return okHttpBuilder;
    }

    /**
     * @return HttpLogginInterceptor
     */
    private HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }
}
