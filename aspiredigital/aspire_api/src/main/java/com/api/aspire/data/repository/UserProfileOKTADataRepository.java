package com.api.aspire.data.repository;

import android.text.TextUtils;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.domain.model.UserProfileOKTA;
import com.api.aspire.domain.repository.UserProfileOKTARepository;

import io.reactivex.Completable;
import io.reactivex.Single;

public class UserProfileOKTADataRepository implements UserProfileOKTARepository {

    private RemoteUserProfileOKTADataStore remote;

    public UserProfileOKTADataRepository(RemoteUserProfileOKTADataStore remote) {
        this.remote = remote;
    }

    @Override
    public Single<UserProfileOKTA> getUserProfileOKTA(String clientIdEmail) {
        return remote.getUserProfile(clientIdEmail)
                .map(userProfileResponse -> {
                    UserProfileOKTA.Provider provider = new UserProfileOKTA.Provider("", "");
                    String question = (userProfileResponse.getCredentials() != null && userProfileResponse.getCredentials().getRecoveryQuestion() != null)
                            ? userProfileResponse.getCredentials().getRecoveryQuestion().getQuestion()
                            : "";
                    UserProfileOKTA.RecoveryQuestion recoveryQuestion = new UserProfileOKTA.RecoveryQuestion(question);
                    UserProfileOKTA.Password password = new UserProfileOKTA.Password();
                    UserProfileOKTA.Credentials credentials = new UserProfileOKTA.Credentials(provider, recoveryQuestion, password);

                    UserProfileOKTA.Profile profileOKTA;
                    if (userProfileResponse.getProfile() == null) {
                        profileOKTA = new UserProfileOKTA.Profile("", "", "", "", "", "", "");
                    } else {
                        UserProfileOKTA.Profile profileTemp = userProfileResponse.getProfile();
                        profileOKTA = new UserProfileOKTA.Profile(
                                TextUtils.isEmpty(profileTemp.getEmail()) ? "" : profileTemp.getEmail(),
                                TextUtils.isEmpty(profileTemp.getLogin()) ? "" : profileTemp.getLogin(),
                                TextUtils.isEmpty(profileTemp.getPartyid()) ? "" : profileTemp.getPartyid(),
                                TextUtils.isEmpty(profileTemp.getUsertype()) ? "" : profileTemp.getUsertype(),
                                TextUtils.isEmpty(profileTemp.getOrganization()) ? "" : profileTemp.getOrganization(),
                                TextUtils.isEmpty(profileTemp.getLastname()) ? "" : profileTemp.getLastname(),
                                TextUtils.isEmpty(profileTemp.getFirstname()) ? "" : profileTemp.getFirstname()
                        );
                    }
                    String id = TextUtils.isEmpty(userProfileResponse.getId()) ? "" : userProfileResponse.getId();
                    String status = TextUtils.isEmpty(userProfileResponse.getStatus()) ? "" : userProfileResponse.getStatus();
                    return new UserProfileOKTA.UserProfileOKTABuilder()
                            .setId(id)
                            .setStatus(status)
                            .setProfile(profileOKTA)
                            .setCredentials(credentials)
                            .createUserProfileOKTA();
                });
    }

    /**
     * Profile OKTA status LOCKED OUT
     * check on flow Sign In and Forgot password
     * */
    public Completable handleCheckStatus(UserProfileOKTA userProfileOKTA) {
        return Completable.create(emit -> {
            String status = userProfileOKTA.getStatus();
            if (isStatusLockOut(status)) {
                emit.onError(new Exception(ErrCode.PROFILE_OKTA_LOCKED_OUT.name()));
            } else {
                emit.onComplete();
            }
        });
    }

    @Override
    public boolean isStatusLockOut(String status){
        return TextUtils.isEmpty(status) || "LOCKED_OUT".equalsIgnoreCase(status);
    }
}
