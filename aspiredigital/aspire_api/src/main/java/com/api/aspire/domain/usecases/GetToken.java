package com.api.aspire.domain.usecases;

import android.text.TextUtils;
import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.exception.BackendException;
import com.api.aspire.data.entity.oauth.GetTokenAspireRequest;
import com.api.aspire.data.entity.oauth.GetTokenAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.restapi.AspireOAuthApi;
import com.api.aspire.data.retro2client.AppHttpClient;
import com.api.aspire.domain.model.AuthData;
import com.google.gson.Gson;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class GetToken {

    private PreferencesStorageAspire preferencesStorage;
    private MapProfileBase mapApp;

    public GetToken(PreferencesStorageAspire preferencesStorage, MapProfileBase mapApp) {
        this.preferencesStorage = preferencesStorage;
        this.mapApp = mapApp;
    }

    public Single<String> getTokenUser(final String username, final String secretKey) {
        return getTokenUser(username, secretKey, false);
    }

    /**
     * same logic with: RemoteProfileDataStoreAspire.refreshTokenAndCallGetProfileSecond
     * */
    private Single<String> getTokenUser(final String username, final String secretKey, boolean isDoubleRequest) {
        return Single.create(e -> {
            Call<GetTokenAspireResponse> requestTokenCall = requestApiGetTokenUser(username, secretKey);
            Response<GetTokenAspireResponse> requestTokenResponse = requestTokenCall.execute();

            if (!requestTokenResponse.isSuccessful()) {
                if (isDoubleRequest) {
                    requestTokenResponse = requestTokenCall.clone().execute();
                    if (!requestTokenResponse.isSuccessful()) {
                        e.onError(new BackendException(requestTokenResponse.errorBody().string()));
                        return;
                    }//-- else continues handle accessToken

                } else {
                    e.onError(new BackendException(requestTokenResponse.errorBody().string()));
                    return;
                }
            }//-- else continues handle accessToken

            String accessToken = requestTokenResponse.body().getAccessToken();
            String tokenType = requestTokenResponse.body().getTokenType();

            if (TextUtils.isEmpty(accessToken) || TextUtils.isEmpty(tokenType)) {
                e.onError(new Exception(requestTokenResponse.body().toString()));
            } else {
                String token = createTokenAndSaveStorage(accessToken, tokenType);
                e.onSuccess(token);
            }
        });
    }

    public String createTokenAndSaveStorage(String accessToken, String tokenType) {
        String token = tokenType + " " + accessToken;
        saveStorageAccessToken(token);
        return token;
    }

    public Call<GetTokenAspireResponse> requestApiGetTokenUser(String username, String secretKey) {
        AspireOAuthApi aspireOAuthApi = AppHttpClient.getInstance().getAspireOAuthApi();

        String clientIdEmail = mapApp.convertClientIdEmail(username);
        GetTokenAspireRequest rq = new GetTokenAspireRequest(clientIdEmail, secretKey);

        return aspireOAuthApi.getTokenUser(rq.getGrantType(),
                rq.getRedirectUri(),
                rq.getScope(),
                rq.getUsername(),
                rq.getSecretKey()
        );
    }

    /**
     * Create new account
     * Check PassCode
     */
    public Single<String> getTokenService() {
        return Single.create(e -> {
            AspireOAuthApi aspireOAuthApi = AppHttpClient.getInstance().getAspireOAuthApi();
            // access token
            GetTokenAspireRequest rq = new GetTokenAspireRequest(mapApp.getTokenServiceUserName(),
                    mapApp.getTokenServiceSecret()
            );
            Call<GetTokenAspireResponse> requestTokenCall = aspireOAuthApi.getTokenService(rq.getGrantType(),
                    rq.getRedirectUri(),
                    rq.getScope(),
                    rq.getUsername(),
                    rq.getSecretKey()
            );
            Response<GetTokenAspireResponse> requestTokenResponse = requestTokenCall.execute();

            if (!requestTokenResponse.isSuccessful()) {
                e.onError(new BackendException(requestTokenResponse.errorBody().string()));
                return;
            }

            String accessToken = requestTokenResponse.body().getAccessToken();
            String tokenType = requestTokenResponse.body().getTokenType();

            if (TextUtils.isEmpty(accessToken) || TextUtils.isEmpty(tokenType)) {
                e.onError(new Exception(requestTokenResponse.body().toString()));
            } else {
                String token = tokenType + " " + accessToken;
                e.onSuccess(token);
            }
        });
    }

    /**
     * save access token in storage device
     */
    private void saveStorageAccessToken(String accessToken) {
        long timeLimit = 30 * 60000L; //-- 30 minutes access token is expire
        Long timeExpiry = System.currentTimeMillis() + timeLimit;
        AuthData authData = new AuthData(accessToken, timeExpiry);

        String authObj = new Gson().toJson(authData);
        preferencesStorage.editor().authToken(authObj).build().save();
    }
    /**
     * @return true: token is Expired, otherwise false: token useful
     */
    private boolean isTokenExpired() {

        AuthData authData = preferencesStorage.authToken();
        if (authData == null || TextUtils.isEmpty(authData.accessToken)) {
            return true;
        }

        long currentTime = System.currentTimeMillis() - authData.timeExpireToken;
        return currentTime > 0; //-- time limit > time current
    }

    public Single<String> refreshToken(final String username, final String secretKey) {
        if (isTokenExpired()) {
            return getTokenUser(username, secretKey, true);
        } else {
            return Single.just(preferencesStorage.authToken().accessToken);
        }
    }

    /**
     * call api revoke token and clear accessToken
     * Error: REVOKE_TOKEN_ERROR
     * */
    public Completable revokeToken(String token) {
        return Completable.create(e -> {
            Response<ResponseBody> responseRevoke = AppHttpClient.getInstance()
                    .getAspireOAuthApi()
                    .revokeToken(token)
                    .execute();
            if (responseRevoke.isSuccessful() && responseRevoke.code() == 200) {
                e.onComplete();
//                Log.d("RevokeToken", "Success \n" + token);
            }else{
                e.onError(new Exception(ErrCode.REVOKE_TOKEN_ERROR.name()));
//                Log.d("RevokeToken", "Fail \n" + token);
            }
        });
    }

    public Completable revokeTokenInApp(){
        return Single.create((SingleEmitter<String> e) -> {
            String token = getAuthTokenCurrent();

            if (TextUtils.isEmpty(token)) {
                e.onError(new Exception("Auth token empty"));
                return;
            }

            //<editor-fold desc="clear reset accessToken">
            AuthData authData = new AuthData("", 0);
            String authObj = new Gson().toJson(authData);
            preferencesStorage.editor().authToken(authObj).build().save();
            //</editor-fold>

            e.onSuccess(token);

        }).flatMapCompletable(this::revokeToken);
    }

    public String getAuthTokenCurrent(){
        AuthData authDataLocal = preferencesStorage.authToken();
        if (authDataLocal == null || TextUtils.isEmpty(authDataLocal.accessToken)) {
            return "";
        }else{
            return authDataLocal.accessToken;
        }
    }

}