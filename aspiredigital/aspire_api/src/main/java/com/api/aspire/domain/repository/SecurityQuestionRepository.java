package com.api.aspire.domain.repository;

import com.api.aspire.domain.model.SecurityQuestion;

import java.util.List;

import io.reactivex.Single;

public interface SecurityQuestionRepository {
    Single<List<SecurityQuestion>> getSecurityQuestions();
}
