package com.api.aspire.domain.usecases;

import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class SaveProfile extends UseCase<Void, SaveProfile.Params> {
    private ProfileAspireRepository profileRepository;

    public SaveProfile(ProfileAspireRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    Single<Void> buildUseCaseSingle(Params params) {
        return null;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
        return profileRepository.saveProfile(params.accessToken, params.profile);
    }

    public static class Params {
        private ProfileAspire profile;
        private String accessToken;

        public Params(ProfileAspire profile, String accessToken) {
            this.profile = profile;
            this.accessToken = accessToken;
        }
    }
}
