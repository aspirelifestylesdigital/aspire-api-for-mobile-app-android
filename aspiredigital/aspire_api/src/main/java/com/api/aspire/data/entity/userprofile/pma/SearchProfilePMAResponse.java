package com.api.aspire.data.entity.userprofile.pma;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchProfilePMAResponse {

	@Expose
	@SerializedName("contacts")
	private Contacts contacts;
	@Expose
	@SerializedName("locations")
	private List<Locations> locations;
	@Expose
	@SerializedName("partyPerson")
	private Partyperson partyperson;
	@Expose
	@SerializedName("partyId")
	private String partyid;

	public Contacts getContacts() {
		return contacts;
	}

	public List<Locations> getLocations() {
		return locations;
	}

	public Partyperson getPartyperson() {
		return partyperson;
	}

	public String getPartyid() {
		return partyid;
	}

	public static class Contacts {
		@Expose
		@SerializedName("phones")
		private List<Phones> phones;
		@Expose
		@SerializedName("emails")
		private List<Emails> emails;

		public List<Phones> getPhones() {
			return phones;
		}

		public List<Emails> getEmails() {
			return emails;
		}
	}

	public static class Phones {
		@Expose
		@SerializedName("phoneNumber")
		private String phonenumber;

		public String getPhonenumber() {
			return phonenumber;
		}
	}

	public static class Emails {
		@Expose
		@SerializedName("emailAddress")
		private String emailaddress;

		public String getEmailaddress() {
			return emailaddress;
		}
	}

	public static class Locations {
		@Expose
		@SerializedName("address")
		private Address address;

		public Address getAddress() {
			return address;
		}
	}

	public static class Address {
		@Expose
		@SerializedName("zipCode")
		private String zipcode;

		public String getZipcode() {
			return zipcode;
		}
	}

	public static class Partyperson {
		@Expose
		@SerializedName("lastName")
		private String lastname;
		@Expose
		@SerializedName("firstName")
		private String firstname;

		public String getLastname() {
			return lastname;
		}

		public String getFirstname() {
			return firstname;
		}
	}

}