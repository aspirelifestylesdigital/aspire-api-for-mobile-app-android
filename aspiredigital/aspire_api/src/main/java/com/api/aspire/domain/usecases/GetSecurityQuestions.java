package com.api.aspire.domain.usecases;

import com.api.aspire.domain.model.SecurityQuestion;
import com.api.aspire.domain.repository.SecurityQuestionRepository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class GetSecurityQuestions extends UseCase<List<SecurityQuestion>, GetSecurityQuestions.Param> {

    private SecurityQuestionRepository challengeQuestionData;

    public GetSecurityQuestions(SecurityQuestionRepository challengeQuestionData) {
        this.challengeQuestionData = challengeQuestionData;
    }

    @Override
    Observable<List<SecurityQuestion>> buildUseCaseObservable(GetSecurityQuestions.Param param) {
        return null;
    }

    @Override
    Single<List<SecurityQuestion>> buildUseCaseSingle(GetSecurityQuestions.Param param) {
        return challengeQuestionData.getSecurityQuestions();
    }

    @Override
    Completable buildUseCaseCompletable(GetSecurityQuestions.Param param) {
        return null;
    }

    public static class Param {

    }
}
