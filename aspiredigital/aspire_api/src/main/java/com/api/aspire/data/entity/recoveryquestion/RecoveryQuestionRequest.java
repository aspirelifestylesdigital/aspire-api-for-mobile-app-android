package com.api.aspire.data.entity.recoveryquestion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecoveryQuestionRequest {

    @Expose
    @SerializedName("recovery_question")
    private RecoveryQuestion recoveryQuestion;
    @Expose
    @SerializedName("password")
    private Password password;

    public RecoveryQuestion getRecoveryQuestion() {
        return recoveryQuestion;
    }

    public void setRecoveryQuestion(RecoveryQuestion recoveryQuestion) {
        this.recoveryQuestion = recoveryQuestion;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public static class RecoveryQuestion {
        @Expose
        @SerializedName("answer")
        private String answer;
        @Expose
        @SerializedName("question")
        private String question;

        public RecoveryQuestion(String question, String answer) {
            this.answer = answer;
            this.question = question;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }
    }

    public static class Password {
        @Expose
        @SerializedName("value")
        private String value;

        public Password(String password) {
            this.value = password;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
