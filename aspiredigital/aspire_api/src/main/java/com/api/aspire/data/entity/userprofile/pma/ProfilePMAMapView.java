package com.api.aspire.data.entity.userprofile.pma;

import android.os.Parcel;
import android.os.Parcelable;

public class ProfilePMAMapView implements Parcelable {

	private String partyId;
	private String firstName;
	private String lastName;
	private String zipCode;
	private String phone;
	private String email;
	//-- default don't have data on PMA
	private boolean agreedNewsLetter;

	public ProfilePMAMapView(String partyId, String firstName, String lastName, String zipCode, String phone, String email) {
		this.partyId = partyId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.zipCode = zipCode;
		this.phone = phone;
		this.email = email;
		this.agreedNewsLetter = false;
	}

	public static ProfilePMAMapView empty() {
		return new ProfilePMAMapView("", "", "","","","");
	}

	public String getPartyId() {
		return partyId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}

	public boolean isAgreedNewsLetter() {
		return agreedNewsLetter;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAgreedNewsLetter(boolean agreedNewsLetter) {
		this.agreedNewsLetter = agreedNewsLetter;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.partyId);
		dest.writeString(this.firstName);
		dest.writeString(this.lastName);
		dest.writeString(this.zipCode);
		dest.writeString(this.phone);
		dest.writeString(this.email);
		dest.writeByte(this.agreedNewsLetter ? (byte) 1 : (byte) 0);
	}

	protected ProfilePMAMapView(Parcel in) {
		this.partyId = in.readString();
		this.firstName = in.readString();
		this.lastName = in.readString();
		this.zipCode = in.readString();
		this.phone = in.readString();
		this.email = in.readString();
		this.agreedNewsLetter = in.readByte() != 0;
	}

	public static final Creator<ProfilePMAMapView> CREATOR = new Creator<ProfilePMAMapView>() {
		@Override
		public ProfilePMAMapView createFromParcel(Parcel source) {
			return new ProfilePMAMapView(source);
		}

		@Override
		public ProfilePMAMapView[] newArray(int size) {
			return new ProfilePMAMapView[size];
		}
	};
}