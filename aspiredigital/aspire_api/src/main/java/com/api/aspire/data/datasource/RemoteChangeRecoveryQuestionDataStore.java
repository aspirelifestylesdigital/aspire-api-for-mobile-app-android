package com.api.aspire.data.datasource;

import android.text.TextUtils;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.entity.recoveryquestion.RecoveryQuestionRequest;
import com.api.aspire.data.entity.recoveryquestion.RecoveryQuestionResponse;
import com.api.aspire.data.retro2client.AppHttpClient;

import io.reactivex.Completable;
import retrofit2.Call;
import retrofit2.Response;

public class RemoteChangeRecoveryQuestionDataStore {

    public Completable changeRecoveryQuestion(String userId, RecoveryQuestionRequest recoveryQuestionRequest) {
        return Completable.create(emitter -> {
            Call<RecoveryQuestionResponse> request = AppHttpClient.getInstance()
                    .getOktaUserApi()
                    .changeRecoveryQuestion(userId, recoveryQuestionRequest);

            Response<RecoveryQuestionResponse> response = request.execute();
            if (response == null || !response.isSuccessful() || response.body() == null) {
                emitter.onError(new Exception(ErrCode.CHANGE_SECURITY_QUESTION_ERROR.name()));
            } else {
                String questionResponse = (response.body().getRecoveryQuestion() == null
                        || TextUtils.isEmpty(response.body().getRecoveryQuestion().getQuestion())
                        ? ""
                        : response.body().getRecoveryQuestion().getQuestion());

                String questionRequest = recoveryQuestionRequest.getRecoveryQuestion() == null
                        ? ""
                        : recoveryQuestionRequest.getRecoveryQuestion().getQuestion();

                if (!TextUtils.isEmpty(questionResponse) && questionResponse.equals(questionRequest)) {
                    emitter.onComplete();
                } else {
                    emitter.onError(new Exception(ErrCode.CHANGE_SECURITY_QUESTION_ERROR.name()));
                }
            }
        });
    }
}
